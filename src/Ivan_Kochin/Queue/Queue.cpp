#include "Queue.h"



void Queue::Put(const TData &Val){
	if(pMem == nullptr)
		SetRetCode(DataNoMem);
	else if (IsFull())
		SetRetCode(DataFull);
	else
	{
		if(Hi==MemSize-1)	Hi=0;
		else	Hi++;
		pMem[Hi]=Val;
		DataCount++;
	}
}

TData Queue:: Get(void){
	TData result = -1;
	if(pMem == nullptr)
		SetRetCode(DataNoMem);
	else if (IsEmpty())
		SetRetCode(DataEmpty);
	else
	{
		if(Li==MemSize-1)  Li=0;
		else	Li++;

		result = pMem[Li];
		DataCount--;
	}
	return result;
}

int Queue:: IsValid()
{
	return 1;
}

void Queue:: Print(){
	if(IsEmpty())
		cout<< "Queue is empty";
	else if (Hi > Li)
		for (int i = Li; i <= Hi;i ++)
			cout<< pMem[i]<< ' ';
	else{
		for (int i = Li; i < MemSize - 1; i++)
			cout<< pMem[i]<< ' ';
		for (int i = 0; i <= Hi; i++)
			cout<< pMem[i]<< ' ';
	}
	cout<<endl;
}