#pragma once
#include "tdataroot.h"
#include <iostream>
/*
PTelem - ��������� �� int ��� �������� �������
TData - int

PTelem PMem - ������ ��� �����
MemSize - ���� ���-�� ��������� (���-�� ���������� ������)
DataCount - ������� �����
MemType - MEM_HOLDER ��� MEM_RENTER ������ �� ����� ��������� ������
RetCode - ��� ����������
*/

using namespace std;

class Queue: public TDataRoot{
	protected:
		int Li,Hi;
	public:
		Queue(int Size = DefMemSize): TDataRoot(Size),Li(0),Hi(-1){}
		virtual void Put(const TData &Val);
		virtual TData Get(void);
		virtual void Print();
		virtual int IsValid();
		};


